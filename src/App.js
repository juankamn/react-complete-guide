// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }
import Expenses from "./components/Expenses/Expenses";
import React, { useState } from "react";
import NewExpense from "./components/NewExpense/NewExpense";

const DUMMY_EXPENSES = [
  {
    id: "e1",
    title: "Toilet Paper",
    amount: 94.12,
    date: new Date(2020, 7, 14),
  },
  { 
    id: "e2", 
    title: "New TV", 
    amount: 799.49, 
    date: new Date(2021, 3, 12) 
  },
  {
    id: "e3",
    title: "Car Insurance",
    amount: 294.67,
    date: new Date(2021, 2, 28),
  },
  {
    id: "e4",
    title: "New Desk (Wooden)",
    amount: 450,
    date: new Date(2021, 6, 12),
  },
  {
    id: "e5",
    title: "Toilet Paper",
    amount: 94.12,
    date: new Date(2022, 8, 14),
  },
  { 
    id: "e6", 
    title: "New Bike", 
    amount: 345.49, 
    date: new Date(2022, 2, 12) 
  },
  {
    id: "e7",
    title: "Car Insurance",
    amount: 294.67,
    date: new Date(2022, 1, 28),
  },
  {
    id: "e8",
    title: "Repair Desk",
    amount: 150,
    date: new Date(2022, 3, 12),
  },
  {
    id: "e9",
    title: "Toilet Paper",
    amount: 94.12,
    date: new Date(2021, 8, 14),
  },
  { 
    id: "e10", 
    title: "New Car", 
    amount: 3699.49, 
    date: new Date(2020, 11, 12) 
  },
  {
    id: "e3",
    title: "Car Insurance",
    amount: 394.67,
    date: new Date(2019, 10, 28),
  },
  {
    id: "e4",
    title: "New Desk (Wooden)",
    amount: 550,
    date: new Date(2019, 9, 12),
  },
];

const App = () => {
  const [expenses, setExpenses] = useState(DUMMY_EXPENSES);

  // Old way/Behind the scenes stuff that are going on
  // return React.createElement(
  //   "div",
  //   {},
  //   React.createElement("h2", {}, "Let's get started!"),
  //   React.createElement(Expenses, {items: expenses}),
  // );

  const addExpenseHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
    // console.log(expense);
  };

  // New way, easy way
  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler}></NewExpense>
      <Expenses items={expenses}></Expenses>
    </div>
  );
};

export default App;
