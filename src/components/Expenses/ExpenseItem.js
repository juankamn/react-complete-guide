import React, { useState } from "react";

import ExpenseDate from "./ExpenseDate";
import Card from "../UI/Card";
import "./ExpenseItem.css";

const ExpenseItem = (props) => {
  // const [title, setTitle] = useState(props.title);
  // console.log("ExpenseItem evaluated by React");

  const date = props.date;
  const title = props.title;
  const amount = props.amount;

  // const clickHandler = () => {
  //   setTitle("I have changed!");
  //   // console.log(title); // show last value because setTitle doesnt update right away, but it scheduled this state update
  // };

  return (
    <li>
      <Card className="expense-item">
        <ExpenseDate date={date} />
        <div className="expense-item__description">
          <h2>{title}</h2>
          <div className="expense-item__price">${amount}</div>
        </div>
        {/* <button onClick={clickHandler}>Change title</button> */}
      </Card>
    </li>
  );
};

export default ExpenseItem;
